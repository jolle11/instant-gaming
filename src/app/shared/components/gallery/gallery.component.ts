import { Component, Input, OnInit } from '@angular/core';
import { gameInterface } from 'src/app/core/models/Igame';

@Component({
    selector: 'app-gallery',
    templateUrl: './gallery.component.html',
    styleUrls: ['./gallery.component.scss'],
})
export class GalleryComponent implements OnInit {
    @Input() list: gameInterface[] = [
        {
            image: 'images/animal-crossing-new-horizons-happy-home-paradise.jpg',
            title: 'Animal Crossing New Horizons',
            price: '50€',
            discount: '20%',
        },
    ];
    constructor() {}

    ngOnInit(): void {}
}
