export interface gameInterface {
    image: string;
    title: string;
    price: string;
    discount: string;
}
