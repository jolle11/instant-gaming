import { Component, OnInit } from '@angular/core';
import { gameInterface } from 'src/app/core/models/Igame';

@Component({
    selector: 'app-home-page',
    templateUrl: './home-page.component.html',
    styleUrls: ['./home-page.component.scss'],
})
export class HomePageComponent implements OnInit {
    games: gameInterface[] = [
        {
            image: 'assets/images/animal-crossing-new-horizons-happy-home-paradise.jpg',
            title: 'Animal Crossing New Horizons',
            price: '50€',
            discount: '20%',
        },
        {
            image: 'assets/images/farming-simulator-22.jpg',
            title: 'Farming Simulator 22',
            price: '20€',
            discount: '50%',
        },
        {
            image: 'assets/images/forza-horizon-5.jpg',
            title: 'Forza Horizon 5',
            price: '70€',
            discount: '10%',
        },
        {
            image: 'assets/images/animal-crossing-new-horizons-happy-home-paradise.jpg',
            title: 'Animal Crossing New Horizons',
            price: '50€',
            discount: '20%',
        },
        {
            image: 'assets/images/farming-simulator-22.jpg',
            title: 'Farming Simulator 22',
            price: '20€',
            discount: '50%',
        },
        {
            image: 'assets/images/forza-horizon-5.jpg',
            title: 'Forza Horizon 5',
            price: '70€',
            discount: '10%',
        },
        {
            image: 'assets/images/animal-crossing-new-horizons-happy-home-paradise.jpg',
            title: 'Animal Crossing New Horizons',
            price: '50€',
            discount: '20%',
        },
        {
            image: 'assets/images/farming-simulator-22.jpg',
            title: 'Farming Simulator 22',
            price: '20€',
            discount: '50%',
        },
        {
            image: 'assets/images/forza-horizon-5.jpg',
            title: 'Forza Horizon 5',
            price: '70€',
            discount: '10%',
        },
    ];

    constructor() {}

    ngOnInit(): void {}
}
